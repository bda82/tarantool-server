# tarantool-server

## Installation requirements

- docker
- docker-compose

## Run application

Just call ```make``` command from project root.

## Project Parts

- ./src/main.py - Python code with Tarantool connector and functions wrapper
- ./src/lua/app.lua - Lua code for tarantool server

Tarantool server contains:

- Data schema for Measurements
- Http server for Measurements requests

## Server routes

- /measurements - Get all measurements
- /measurements/:id - Get measurement by id