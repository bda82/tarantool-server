#!/usr/bin/env tarantool

-- Data storage

box.cfg{listen=3301}

function log(s)
    print(s)
    io.flush()
end

local Measurements ={}

function Measurements.create_database()
    local Model = {}

    Model.ID = 1
    Model.TS = 2
    Model.SENSOR = 3
    Model.DATA = 4

    Model.ID_INDEX = "idIndex"
    Model.TS_INDEX = "tsIndex"
    Model.SENSOR_INDEX = "sensor_index"

    Model.SEQUENCE = "idSequence"

    local measurementsSpace = box.schema.space.create("measurements", {
        if_not_exists = true,
        format = {
            { name = "id", type = "number" },
            { name = "ts", type = "number" },
            { name = "sensor", type = "string" },
            { name = "data", type = "number" }
        }
    })

    measurementsSpace:create_index(Model.ID_INDEX, {
        type = 'tree',
        parts = {Model.ID, "number"},
        if_not_exists = true
    })

    measurementsSpace:create_index(Model.TS_INDEX, {
        type = 'tree',
        parts = {Model.TS, "number"},
        if_not_exists = true,
    })

    measurementsSpace:create_index(Model.SENSOR_INDEX, {
        type = 'tree',
        parts = {Model.SENSOR, "string"},
        if_not_exists = true,
        unique = false
    })
end

function Measurements.measurements_generate_next_id()
    return box.sequence.idSequence:next()
end

box.schema.sequence.create('idSequence', {
    if_not_exists = true,
    min = 1,
    step = 1
})

function measurements_insert(ts, sensor, data)
    local id = Measurements.measurements_generate_next_id()
    log('Generate ID = ' .. id)
    log('Insert Measurement, ts = ' .. ts .. ' from sendor = ' .. sensor)
    return box.space.measurements:insert({id, ts, sensor, data})
end

function measurements_get_by_element_id(id)
    log('Get measurements_get_by_element_id request, id = ' .. id)
    return box.space.measurements.index.idIndex:select({id}, {iterator = box.index.EQ})
end

function measurements_select_all()
    return box.space.measurements.index.idIndex:select({})
end

Measurements.create_database()

-- Server

local function IndexHandler(req)
    local MS = measurements_select_all()
    local responseData = {}
    for id, mesData in pairs(MS) do
        table.insert(responseData, { ["id"] = id, ["measusementData"] = mesData })
    end
    return req:render{ json = responseData }
end

local function IdHandler(req)
    local measurementId = tonumber(req:stash('id'))
    local responseData = measurements_get_by_element_id(measurementId)
    return req:render{ json = { ['measurement'] = responseData } }
end

local server = require('http.server').new(nil, 3303) -- listen *:3303
server:route({ path = '/measurements' }, IndexHandler)
server:route({ path = '/measurements/:id' }, IdHandler)
server:start()