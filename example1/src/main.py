import os
import time
import random
import logging
import tarantool
import datetime


class Config:
    host = os.environ.get('TARANTOOL_HOST', 'localhost')
    port = os.environ.get('TARANTOOL_PORT', 3301)
    user = os.environ.get('TARANTOOL_USER_NAME', 'root')
    password = os.environ.get('TARANTOOL_USER_PASSWORD', 'password')
    slab_alloc_arena = os.environ.get('TARANTOOL_SLAB_ALLOC_ARENA', 0.1)
    space_name_measurements = "measurements"


class TarantoolConnector:
    def __init__(self, space_name):
        self.__config = Config()
        self.__logger = logging.getLogger("TarantoolConnector")
        self.__space_name = space_name
        self.connection = None
        try:
            self.connection = tarantool.connect(host=self.__config.host,
                                                  port=self.__config.port,
                                                  user=self.__config.user,
                                                  password=self.__config.password)
        except Exception as ex:
            self.__logger.error(f'Tarantool connection error = {ex}')
            raise Exception("Tarantool connection error")


class MeasurementsModel:
    def __init__(self):
        self.__config = Config()
        self.__logger = logging.getLogger("TarantoolConnector")
        self.__space_name = self.__config.space_name_measurements
        self.__tarantool_connector = TarantoolConnector(self.__space_name)
   
    def insert(self,
               ts: int,
               sensor: str,
               data: float):
        if self.__tarantool_connector is not None:
            try:
                res = self.__tarantool_connector.connection.call(f'{self.__space_name}_insert',
                                                         (ts,
                                                         sensor,
                                                         data))
                return res
            except Exception as ex:
                raise ex
        raise Exception("Tarantool connection error")

    def get_by_id(self, element_id):
        if self.__is_connected:
            sel = self.__tarantool_connector.connection.call(
                f'{self.__space_name}_get_by_element_id', 
                [element_id])
            return self.__to_python(sel)
        raise Exception("Tarantool connection error")

    def select_all(self):
        if self.__is_connected:
            sel = self.__tarantool_connector.connection.select(self.__space_name, [])
            return self.__to_python(sel)
        raise Exception("Tarantool connection error")

    @property
    def __is_connected(self):
        res = self.__tarantool_connector is None
        if not res:
            return True
        return False

    def __to_python(self, response):
        data = self.__extract_data(response)
        python_response = []
        for d in data:
            if len(d) == 4:
                template = dict()
                template["id"] = int(d[0])
                template["ts"] = int(d[1])
                template["sensor"] = str(d[2])
                template["data"] = float(d[3])
                python_response.append(template)
            else:
                raise Exception("Tarantool response data length error")
        return python_response

    def __extract_data(self, response):
        data = response.data
        response_type = self.__check_response_type(data)
        if response_type == 'empty':
            return []
        if response_type == 'double_list':
            data = data[0]
        return data

    @staticmethod
    def __check_response_type(data):
        if isinstance(data, list):
            if len(data) > 0:
                d1 = data[0]
                if isinstance(d1, list):
                    if len(d1) > 0:
                        d2 = d1[0]
                        if isinstance(d2, list):
                            if len(d2) > 0:
                                return 'double_list'
                        else:
                            return 'one_list'
        return 'empty'


def create_data_set():
    data_set = []
    for i in range(10):
        dts = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
        dt_obj = datetime.datetime.strptime(dts, '%Y-%m-%d %H:%M:%S.%f')
        millisec = dt_obj.timestamp() * 1000
        ts = int(millisec)
        sensor = f"sensor-{i}"
        data = i / random.randint(2, 100)
        data_set.append(
            (ts, sensor, data)
        )
        time.sleep(0.01)
    return data_set


M = MeasurementsModel()

data_set = create_data_set()

for d in data_set:
    M.insert(d[0], d[1], d[2])