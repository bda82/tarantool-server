#!/usr/bin/env tarantool

local log = require "log"
local Users = require "users"
local Server = require "server"

box.cfg{ listen = 3301 }

Users.create_space()
Server:start()