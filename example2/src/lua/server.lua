#!/usr/bin/env tarantool

local log = require "log"
local Users = require "users"

box.cfg{ listen = 3301 }


local function UsersAllHandler(req)
    local all = Users.get_all()
    local usersData = {}
    for index, data in pairs(all) do
        table.insert( usersData, { ["index"] = index, ["data"] = data })
    end
    local res = req:render{ json = usersData }
    res.status = 200
    return res
end

local function UsersInsert(req)
    local data = req:json()
    local rdata = Users.insert(data["ts"], data["email"], data["token"])
    local res = req:render{ json = rdata }
    res.status = 200
end

local function UsersIDHandler(req)
    local userid = req:stash('id')
    local usersData = Users.get_by_id(userid)
    local res = req:render{ json = { ["data"] = usersData } }
    res.status = 200
    return res
end

local function UsersUpdate(req)
    local userid = req:stash('id')
    local data = req:json()
    local rdata = Users.update(userid, data["ts"], data["email"], data["token"])
    local res = req:render{ json = rdata }
    res.status = 200
end

local function UsersDelete(req)
    local userid = req:stash('id')
    local rdata = Users.delete(userid)
    local res = req:render{ json = rdata }
    res.status = 200
end

log.info('[Server] Server Initialized')
local Server = require("http.server").new(nil, 3303, {
    log_requests = true,
    log_errors = true,
})

log.info('[Server] Routes Initialized')
Server:route( { method = "GET",    path = "/users" }, UsersAllHandler )
Server:route( { method = "POST",   path = "/users" }, UsersInsert )
Server:route( { method = "GET",    path = "/users/:id" }, UsersIDHandler )
Server:route( { method = "PUT",    path = "/users/:id" }, UsersUpdate )
Server:route( { method = "DELETE", path = "/users/:id"}, UsersDelete )

return Server