#!/usr/bin/env tarantool

local log = require "log"
local uuid = require "uuid"

box.cfg{ listen = 3301 }

local Users = {}

function Users.create_space()
    log.info('[Users] Create Space if not exists')
    local userSpace = box.schema.space.create("users", {
        if_not_exists = true,
        format = {
            { name = "id", type = "string" },
            { name = "ts", type = "number" },
            { name = "email", type = "string" },
            { name = "token", type = "string" },
        }
    })

    userSpace:create_index("primary", {
        type = "tree", 
        parts = { 1, "string" },
        if_not_exists = true,
        unique = true
    })

    userSpace:create_index("tsindex", {
        type = "tree",
        parts = { 2, "number" },
        if_not_exists = true,
        unique = false
    })

    userSpace:create_index("emailindex", {
        type = "tree",
        parts = { 3, "string" },
        if_not_exists = true,
        unique = true
    })

    userSpace:create_index("tokenindex", {
        type = "tree",
        parts = { 4, "string" },
        if_not_exists = true,
        unique = true
    })
end

function Users.generate_uuid()
    return uuid.str()
end

function Users.insert(ts, email, token)
    local newid = Users.generate_uuid()
    
    log.info("[Users] insert: " ..
            " id = " .. newid .. 
            " ts = " .. ts ..
            " email = " .. email ..
            " token = " .. token)
    
    return box.space.users:insert( { newid, ts, email, token } )
end

function Users.get_all()
    log.info("[Users] get_all")

    return box.space.users.index.primary:select( { } )
end

function Users.get_by_id(id)
    log.info("[Users] get_by_id: " .. id)

    return box.space.users.index.primary:select( { id } )
end

function Users.update(id, ts, email, token)
    log.info("[Users] update: " ..
            " id = " .. id .. 
            " ts = " .. ts ..
            " email = " .. email ..
            " token = " .. token)

    return box.space.users:update(id, {
        { '=', 2, ts },
        { '=', 3, email },
        { '=', 4, token }
    })
end

function Users.delete(id)
    log.info("[Users] delete: " .. id)

    return box.space.users:delete(id)
end

return Users